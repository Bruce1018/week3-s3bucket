import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class Week3S3BucketStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'Week3S3BucketQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
    
    //create a S3 bucket 
    const bucket = new cdk.aws_s3.Bucket(this, "MyBucket", {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED
      //removalPolicy: cdk.RemovalPolicy.DESTROY
      //autoDeleteObjects: true
    });

  }
}
