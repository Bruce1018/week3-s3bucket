# Welcome to Bruce week3 project for IDS 721

Here are three requirements 

## 1. Create S3 bucket using AWS CDK

- Create an empty directory and name it. The name of directoty will be the name of `cdk project`
- In that directory, run `cdk init app --language typescript`

Then, you will see the project with `READMD` file

![截屏2024-02-16 20.02.39.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/2331dba2908b4a9488e179c515fb3995~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1724&h=1138&s=320844&e=png&b=1b1b1b)

## 2. Use CodeWhisperer to generate CDK code

- Initialize CodeWhisperer on VScode by downloading `AWS Toolkit` extension
- Use CodeWhisperer to modify `lib/week3-s3bucket-stack.ts`
- As you can see, when you type your comment in the file, CodeWhisperer will help you generate the code.

![截屏2024-02-16 19.47.15.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d70058b7c65f494e9d83f8837e32588a~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1280&h=484&s=35720&e=png&b=1d1d1d)

![截屏2024-02-16 19.47.23.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/f10bbe9d5cea42019f84e478b4d5676b~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1266&h=450&s=45414&e=png&b=1d1d1d)


- Press `tab` to get the code and modify the bucket name  
**For Deliverables**
``` tsx
    //create a S3 bucket 
    const bucket = new cdk.aws_s3.Bucket(this, "MyBucket", {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED
      //removalPolicy: cdk.RemovalPolicy.DESTROY
      //autoDeleteObjects: true
    });
   ```
   
## 3. Add bucket properties like versioning and encryption

- Run `cdk bootstrap`. Due to last week assignment, it knows my AWS account already.

![截屏2024-02-16 20.08.36.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/6fb2a21f35144c6493c525d74fac5fee~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=2264&h=372&s=111075&e=png&b=171717)

- Run `npx cdk deploy` to deploy on AWS 


![截屏2024-02-16 20.09.41.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/a68bc23a94424c5baddc20a5376ca65f~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1986&h=384&s=76144&e=png&b=181818)

**For Deliverables**

Here is the screenshot for my bucekt and properties


![截屏2024-02-16 19.54.01.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/08b83e592d494105bd520432031b8146~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=1876&h=1106&s=216707&e=png&b=fefefe)

![截屏2024-02-16 19.56.12.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7f4a08c2a7864502ad41b073794794ab~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=2024&h=1098&s=186998&e=png&b=fdfdfd)

![截屏2024-02-16 20.12.12.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/399644c59f5b4a35b8bc5f4e2691cdfb~tplv-k3u1fbpfcp-jj-mark:0:0:0:0:q75.image#?w=2834&h=1236&s=399141&e=png&b=fdfdfd)
